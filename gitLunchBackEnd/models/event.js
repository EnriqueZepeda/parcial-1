var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('basedatos', 'root', 'chinchan123.', {
  host: mysql_host,
  dialect: 'mysql'
});


var event = sequelize.define('events', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	"place"				: { type: Sequelize.STRING },
	"location"			: { type: Sequelize.JSON },
	"datetime"			: { type: Sequelize.DATE },
	"max_guests"		: { type: Sequelize.INTEGER },
	"current_guests"	: { type: Sequelize.INTEGER },
	"name"				: { type: Sequelize.STRING },
	"admin"				: { type: Sequelize.INTEGER },
	"date_created"		: { type: Sequelize.DATE },
	"description"		: { type: Sequelize.STRING },
	"status"			: { type: Sequelize.STRING },
	"deadline"			: { type: Sequelize.DATE }

});

event.sync();
module.exports = event;