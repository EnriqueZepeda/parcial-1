var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('basedatos', 'root', 'chinchan123.', {
  host: mysql_host,
  dialect: 'mysql'
});


var products = sequelize.define('products', {
	"id"				: { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true },
	"name"				: { type: Sequelize.STRING },
	"description"				: { type: Sequelize.STRING },
	"price"				: { type: Sequelize.STRING },
	"brand"				: { type: Sequelize.STRING },
	"portion"				: { type: Sequelize.STRING },
	"ingredients"				: { type: Sequelize.STRING }

});

products.sync();
module.exports = products;